var fs            = require('fs');
var path          = require('path');
var gulp          = require('gulp');
var watch         = require('watch');
var map           = require('map-stream');
var through       = require('through');
var browserSync   = require('browser-sync');
var child_process = require('child_process');
var console       = require('console');

var argv = require('yargs').argv;

var sass = require('gulp-sass');
var gutil = require('gulp-util');
var bless = require('gulp-bless');
var insert = require('gulp-insert');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var replace = require('gulp-replace');
var uglify = require('gulp-uglifyjs');
var minifycss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');

var runSequence = require('run-sequence');
var reload      = browserSync.reload;
var package = require('./package.json');

var defaultAppName = argv.name ? argv.name : 'app';
var createRTL = argv.rtl ? true : false;
var production = argv.production ? true : false;
var port = argv.port ? argv.port : 80;

/* file patterns to watch */
var paths = {
    index: ['server.js'],
    scss: ['public/scss/*.scss']
};

var banner = function() {
    return '/*! '+package.name+' - v'+package.version+' - '+gutil.date(new Date(), "yyyy-mm-dd")+
        ' [copyright: '+package.copyright+']'+' */';
};

function logData(data) {
    gutil.log(
        gutil.colors.bold(
            gutil.colors.blue(data)
        )
    );
}

logData('Name : '+defaultAppName);
logData('RTL  : '+ (createRTL ? 'yes':'no'));
logData('PORT : '+ port);
logData('Environment : '+ (production ? 'Production':'Development'));

/* ---------------------------------- */
/* --------- BEGIN APP:SASS --------- */
/* ---------------------------------- */
gulp.task('sass:app', function() {
    return gulp.src('public/scss/*.scss')
        .pipe(sass({
            // sourceComments: 'normal' // uncomment when https://github.com/sass/node-sass/issues/337 is fixed
        }))
        .pipe(autoprefixer('last 2 versions', '> 1%', 'ie 9'))
        .pipe(insert.prepend(banner()+'\n'))
        .pipe(insert.prepend('@charset "UTF-8";\n'))
        .pipe(gulp.dest('public/css/'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('minifycss:app', function() {
    return gulp.src(['public/css/'+defaultAppName+'/raw/ltr/*.css'])
        .pipe(minifycss())
        .pipe(gulp.dest('public/css/'+defaultAppName+'/min/ltr'));
});


gulp.task('bless:app', function() {
    return gulp.src('public/css/'+defaultAppName+'/min/ltr/*.css')
        .pipe(bless())
        .pipe(insert.prepend(banner()+'\n'))
        .pipe(insert.prepend('@charset "UTF-8";\n'))
        .pipe(gulp.dest('public/css/'+defaultAppName+'/blessed/ltr'));
});

/* -------------------------------- */
/* --------- END APP:SASS --------- */
/* -------------------------------- */


gulp.task('uglify:app', ['react:concat'], function() {
    return gulp.src('public/js/'+defaultAppName+'/'+defaultAppName+'.js')
        .pipe(uglify(''+defaultAppName+'.min.js', {
            preserveComments: false,
            compress: {
                warnings: false
            }
        }))
        .pipe(insert.prepend(banner()))
        .pipe(gulp.dest('public/js/'+defaultAppName+'/'));
});
/* ------------------------------- */
/* ---------- END APP:JS --------- */
/* ------------------------------- */


/* --------------------------------- */
/* --------- BEGIN EXPRESS --------- */
/* --------------------------------- */
var child = null, browserSyncConnected = false;
gulp.task('express', function() {
  if(child) child.kill();
  
  child = child_process.spawn(process.execPath, ['./keystone.js'], {
    env: {
      NODE_ENV: process.env.NODE_ENV || 'development',
      PORT: 80,
    },
    stdio: ['ipc']
  });
  
 
  child.stdout.on('data', function(data) {
      gutil.log(data.toString().trim());
  });
  
  child.stderr.on('data', function(data) {
       gutil.log(data.toString().trim());
       browserSync.notify('ERROR: ' + data.toString().trim(), 5000);
  });
  
  child.on('message', function(m) {
    console.log('HELLO');

  	if(m === 'CONNECTED' && !browserSyncConnected) {
       gutil.log(gutil.colors.bgMagenta(gutil.colors.white('Server spawned! Starting proxy...')));
       browserSync({
         proxy: '0.0.0.0:80',
         port: 8085
       }, function() {
         browserSyncConnected = true;
       });
     } else {
       browserSync.notify(m, 5000);
     }
  });
  child.send('CONNECTED');
});
 
process.on('uncaughtException', function(err) {
       if(child) child.kill();
    throw new Error(err);
});
/* ------------------------------- */
/* --------- END EXPRESS --------- */
/* ------------------------------- */



/* ------------------------------- */
/* -------- BEGIN NOTIFY --------- */
/* ------------------------------- */

gulp.task('notify', function() {
    browserSync.notify('Live reloading ...');
});

/* ------------------------------- */
/* ---------- END NOTIFY --------- */
/* ------------------------------- */


/* ------------------------------------ */
/* -------- BEGIN BROWSERSYNC --------- */
/* ------------------------------------ */

var createMonitor = function() {
    var callback = function(f) {
        browserSync.reload(f);
    };

    return function(p) {
        watch.createMonitor(p, function(m) {
            m.on('created', callback);
            m.on('changed', callback);
            m.on('removed', callback);
        });
    }
}

if(!production) {
    var m = createMonitor();
}

/* ------------------------------------ */
/* ---------- END BROWSERSYNC --------- */
/* ------------------------------------ */




/* ------------------------------ */
/* --------- GULP TASKS --------- */
/* ------------------------------ */
gulp.task('uglify', ['uglify:app']);
gulp.task('minifycss', ['minifycss:app']);
gulp.task('build:css', ['sass:app']);

gulp.task('build:dev', ['build:css', 'build:css']);
gulp.task('build:dist', ['minifycss', 'bless', 'uglify']);

if(production) {
    logData('Building please wait...');
    gulp.task('default', function(callback) {
        runSequence('build:css', 'build:essentials', 'minifycss', 'bless', 'uglify', function() {
            callback();
            gutil.log(
                gutil.colors.bgMagenta(
                    gutil.colors.red(
                        gutil.colors.bold('[          COMPLETED BUILD PROCESS          ]')
                    )
                )
            );
        });
    });
} else {
    gulp.task('default', function(callback) {
        runSequence('build:css',['express', 'watch'], callback);
    });
}

/*BEGIN: ALIASES FOR CERTAIN TASKS (for Watch)*/
gulp.task('build:css:watch', ['build:css'], ready);
gulp.task('express:watch', ['express'], ready);
gulp.task('rebuild:css', ['build:css'], ready);
/*END: ALIASES*/

gulp.task('watch', function() 
{
    gulp.watch(paths.index, ['express:watch']);
    gulp.watch(paths.scss, ['rebuild:css']);
    gulp.watch(paths.scss,['notify']);
    gulp.watch(['templates/views/*','templates/views/**/*'],reload);
    gulp.watch(['keystone.js', 'routes/*.js', 'routes/**/*.js', 'models/**/*.js'], ['express']);
});

function ready() {
    gutil.log(
        gutil.colors.bgMagenta(
            gutil.colors.white(
                gutil.colors.bold('[          STATUS: READY          ]')
            )
        )
    );
}
