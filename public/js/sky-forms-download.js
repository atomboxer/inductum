/* 
 Assan v1.3
 Sky forms custom js
 */

$(function()
			{
				// Validation
				$("#sky-form-download").validate(
				{					
					// Rules for form validation
					rules:
					{
						name:
						{
							required: false
						},
						email:
						{
							required:true,
							email:true
							
						},
						os:
						{
							required: true,
						},
						version:
						{
							required: true,
						},
						agree:
						{
							required: true
						}
					},
										
					// Messages for form validation
					messages:
					{
						name:
						{
							required: 'Please enter your name',
						},
						os:
						{
							required:'Please select a platform'
						},
						version:
						{
							required:'Please select a version'
						},
						agree:
						{
							required:'Terms must be accepted'
						}
					},
										
					// Ajax form submition					
					submitHandler: function(form)
					{
						
						$.fileDownload($(form).prop('action'), {
        						httpMethod: "POST",
		        				data: $(form).serialize(),
		        				prepareCallback: function (url) { 
		        				     $('#sky-form-download button[type="submit"]').attr('disabled', true);
		        				     $('#sky-form-download button[type="submit"]').text('Downloading...');	
		        				},

            					successCallback: function (url) { 
            							$("#sky-form-download").addClass('submited');
            							$("#licence").remove();
            					},

					            failCallback: function (responseHtml, url, error) { 
					                  $('#sky-form-download button[type="submit"]').text('Failed!');	
					            },

    					});
					},
					
					// Do not change code below
					errorPlacement: function(error, element)
					{
						error.insertAfter(element.parent());
					}
				});
			});
                        
                        
                        
                        
                        
                        