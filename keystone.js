// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

var express = require('express');

var app = express();

// Require keystone
var keystone = require('keystone');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Accept, Content-Type, api_key, Authorization");
  res.header("Access-Control-Allow-Methods","POST, GET, OPTIONS, PUT");
  next();

});

keystone.set('app', app);


var handlebars = require('express-handlebars');
var cloudinary = require('cloudinary');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({

	'name': 'inductum',
	'brand': 'inductum',
	
	'less': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'hbs',
	
	'custom engine': handlebars.create({
		layoutsDir: 'templates/views/layouts',
		partialsDir: 'templates/views/partials',
		defaultLayout: 'homepage',
		helpers: new require('./templates/views/helpers')(),
		extname: '.hbs'
	}).engine,
	
	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'cookie secret': 'd:Khb"RUP7R|$mvLB[bt9bbZssa#-O!6~e``.93U/u?r7-&!z]evzTnEU3QRvha4',
	'cloudinary config': 'cloudinary://875611885485837:80ccCDnjgq8fyGkir2mUGUWyybk@dqhs4wowd',
	
	'ssl':true,
	'ssl key':  '/etc/letsencrypt/live/inductum.com/privkey.pem',
	'ssl cert': '/etc/letsencrypt/live/inductum.com/fullchain.pem',
	'ssl ca': '/etc/letsencrypt/live/inductum.com/chain.pem',
	'ssl port': 443
});

// Load your project's Models

keystone.import('models');

// set up a route to redirect http to https
app.get('*',function(req,res, next){
	if(req.secure){
        // OK, continue
        return next();
     };
    res.redirect('https://inductum.com'+req.url)
})

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

// Load your project's Routes

keystone.set('routes', require('./routes'));

// Setup common locals for your emails. The following are required by Keystone's
// default email templates, you may remove them if you're using your own.

// Configure the navigation bar in Keystone's Admin UI

keystone.set('nav', {
	'posts': ['posts', 'post-categories'],
	'enquiries': 'enquiries',
	'users': 'users',
	'download_atombox': 'downloads',
	'download_erlang': 'DownloadErlang',
	'hits': 'hits'
});

cloudinary.config({ 
  cloud_name: 'dqhs4wowd', 
  api_key: '875611885485837', 
  api_secret: '80ccCDnjgq8fyGkir2mUGUWyybk' 
});

// Start Keystone to connect to your database and initialise the web server

keystone.start();

process.send('CONNECTED');
