/**
 * This file is where you define your application routes and controllers.
 * 
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 * 
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 * 
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 * 
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 * 
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);
var console = require("console");

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views')
};

var captcha = require('captcha');

// Setup Route Bindings
exports = module.exports = function(app) {

	// Views
	app.get('/', routes.views.index);
	app.get('/atombox', routes.views.atombox);
	app.get('/erlang', routes.views.erlang);
	app.all('/atombox/download', routes.views.download_atombox);
	app.all('/erlang/download', routes.views.download_erlang);
	
	//app.get('/captcha.jpg', captcha.generate());
	var params={
       "url":"/captcha.jpg",
       //"color":"#ffffff",// can be omitted, default 'rgb(0,100,100)'
       "background":"#ffffff",// can be omitted, default 'rgb(255,200,150)'
       "lineWidth" : 4, // can be omitted, default 8
       "fontSize" : 40, // can be omitted, default 80
       "codeLength" : 4, // length of code, can be omitted, default 6
       //"canvasWidth" : 150,// can be omitted, default 250
       //"canvasHeight" : 50,// can be omitted, default 150
    }
    app.use(captcha(params)); // captcha params
	app.get('/blog/:category?', routes.views.blog);
	app.get('/blog/post/:post', routes.views.post);
	app.get('/capcha_check', function(req, res, next) {
		//returns true is valid - the captcha value must sent in the captch field 
		//the error message is in the errDesc key of the captcha object
		console.log(req.query.captcha, 'not ', req.session.captcha)
		
		if (req.query.captcha !== req.session.captcha) {
			
		    return res.status(200).send('false');
		}
        return res.status(200).send('true');
	});
	
	app.all('/contact', routes.views.contact);

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);
};
