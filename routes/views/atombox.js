var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res);
	var locals = res.locals;
	// Init locals
	locals.section = 'atombox';
	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.atombox = true;
	
	// Render the view
	view.render('atombox');
};
