var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');
var Hits = keystone.list('Hits');
var satelize = require('satelize');

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals; // Set locals

	locals.section = 'contact';
	locals.formData = req.body || {};
	locals.validationErrors = {};
	locals.enquirySubmitted = false;

	var newEnquiry = new Enquiry.model(req),
		updater = newEnquiry.getUpdateHandler(req);

	view.on('post', function(next) {
		updater.process(req.body, {
			flashErrors: true,
			fields: 'name,email,subject,message',
			errorMessage: 'There was a problem submitting your enquiry:'
		}, function(err) {
			if (err) {
				locals.validationErrors = err.errors;
				console.log(err);
				next();
				return;
			}

			var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
			satelize.satelize({
				ip: ip,
				timeout: 10000
			}, function(err, geoData) {
				if (err) {;
				}

				var obj = {};
				try {
				if (geoData != undefined)
					obj = JSON.parse(geoData);
				} catch(e) {
					geoData = undefined;
				}
				
				//update the Hits
				var hit_object = {
					name: req.body.name,
					email: req.body.email,
					slugs: 'contact',
					newsletter: req.body.newsletter,
					country: obj.country,
					isp: obj.isp,
					ip: ip
				}

				var newHit = new Hits.model(req);
				var hitsAdapter = newHit.getUpdateHandler(hit_object);

				hitsAdapter.process(hit_object, {
					fields: 'name, email, slugs, newsletter, country, isp, ip',
					flashErrors: true
				}, function(err) {
					if (err) {
						locals.validationErrors = err.errors;
						console.log(err);
						next();
					}
					console.log('all good!');
					next();
				});
			});
		})
	});

	// Render the view
	view.render('contact');
};
