var keystone = require('keystone');
var async = require('async');
var _ = require('underscore');
var Download = keystone.list('Download');
var satelize = require('satelize');

var Hits = keystone.list('Hits');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;
	// Init locals
	locals.section = 'download_atombox';

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.download_atombox = true;

	locals.data = {};
	locals.json = "";
	locals.os = [];
	locals.version = [];

	var newDownload = new Download.model(req),
		updater = newDownload.getUpdateHandler(req);

	view.on('post', function(next) {
		var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
		satelize.satelize({
			ip: ip,
			timeout: 10000
		}, function(err, geoData) {
			if (err) {;
			}

			var obj = {};
			if (geoData != undefined) {
				try {
				    obj = JSON.parse(geoData);
				} catch(e){}
			}

			var hit_object = {
				name: req.body.name,
				email: req.body.email,
				slugs: req.body.os + "~" + req.body.version,
				newsletter: req.body.newsletter,
				country: obj.country,
				isp: obj.isp,
				ip : ip
			};

			var newHit = new Hits.model(req);
			var hitsAdapter = newHit.getUpdateHandler(hit_object);

			hitsAdapter.process(hit_object, {
				fields: 'name, email, slugs, newsletter, country, isp, ip',
				flashErrors: true
			}, function(err) {
				if (err) {
					locals.validationErrors = err.errors;
					console.log(err);
					next();
				}
				else {
					//
					//  Lets get the download
					//
					res.cookie('fileDownload', 'true', {
						path: "/"
					});
					keystone.list('Download').model.findOne({
						os: req.body.os,
						version: req.body.version
					}, function(err, it) {
						if (err) {
							console.log(err);
						}
						else {
							if (it == null)
								next();
							else {
								var filename = it.file.path + "/" + it.file.filename;

								res.download(filename, it.file.originalname, function(err) {
									if (err) {
										locals.validationErrors = err;
										console.log(err);
									}
									else {
										console.dir(it);
										//now update the hits member
										it.hits++;
										keystone.list('Download').model.update({_id: it._id},{'hits':it.hits}, function(err) {
											if (err)
												console.log(err);
										});
									}
								});
							}
						}
					});
				}
			});
		});
	});


	// updater.process(req.body, {
	// 	flashErrors: true,
	// 	fields: 'name, email, os, version',
	// 	errorMessage: 'There was a problem submitting your enquiry:'
	// }, function(err) {
	// 	if (err) {
	// 		locals.validationErrors = err.errors;
	// 		console.log(err);
	// 		next();
	// 	}
	// 	else {
	// 		locals.enquirySubmitted = true;
	// 		return res.send('true');
	// 		//next();
	// 	}
	// })


	// Load all categories
	view.on('init', function(next) {

		keystone.list('Download').model.find().sort('name').exec(function(err, results) {

			if (err || !results.length) {
				return next(err);
			}

			// Load the counts for each category
			async.each(results, function(item, next) {
				if (locals.data[item.os] == undefined)
					locals.data[item.os] = [];

				locals.data[item.os].push(item.version);
				locals.data[item.os] = _.union(locals.data[item.os]);

				locals.json = JSON.stringify(locals.data);
				locals.os.push(item.os);
				locals.version.push(item.version);
				next();

			}, function(err) {
				next(err);
			});

			locals.os = _.union(locals.os);
			locals.version = _.union(locals.version);
		});
	});

	// Render the view
	view.render('download_atombox');
};
