var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Enquiry Model
 * =============
 */

var Enquiry = new keystone.List('Enquiry', {
	nocreate: true,
	noedit: false,
	nodelete:false,
	//map:{name:'nam'},
	autokey: { path: 'slug', from: 'email', unique: true }
});

Enquiry.add({
	name: { type: Types.Name, required: false },
	email: { type: Types.Email, required: true },
	subject: { type:String },
	message: { type: Types.Markdown, required: false },
	createdAt: { type: Date, default: Date.now }
});

Enquiry.defaultSort = '-createdAt';
Enquiry.defaultColumns = 'name, email, subject, message, createdAt';
Enquiry.register();
