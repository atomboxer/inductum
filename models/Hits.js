
var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Hits Model
 * =============
 */

var Hits = new keystone.List('Hits', {
	map: { name: 'name' },
    autokey: { path: 'slug', from: 'name email country'},
    nocreate:true
});

Hits.add({
	name      : { type: String},
	email     : { type: Types.Email},
	slugs     : { type: String, many:true},
	newsletter: { type: Boolean},
	country   : { type: String},
	ip        : { type: String},
	isp       : { type: String},
	datetime  : { type: Types.Datetime, default: Date.now }
});

Hits.defaultColumns = 'name, email, slugs, newsletter, country, isp, ip, datetime';
Hits.register();
