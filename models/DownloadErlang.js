
var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Download Model
 * =============
 */

var DownloadErlang = new keystone.List('DownloadErlang', {
	map: { name: 'name' },
    autokey: { path: 'slug', from: 'os version', unique: true }
});

DownloadErlang.add({
	name   : { type: String, required:true},
	file: { type: Types.LocalFile, default:null, dest:'public/erl_downloads',  filename: function(item, file) {
		return item.id+"."+file.extension
	}, required: true, index:true},

	version: { type: Types.Select, numeric:true, options:'0.1' , default:'0.1', required:true, index:true},
	os:      { type: Types.Select, options:"Erlang OTP 18 (64 bit-ST) TNS/E (H and J)", default: "Erlang OTP 18 (64 bit-ST) TNS/E (H and J)", required:true, index:true},
	hits:    { type: Types.Number, default:0}
//	
});

DownloadErlang.defaultColumns = 'name, file.originalname, version, os, hits';
DownloadErlang.register();
