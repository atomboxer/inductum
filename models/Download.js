
var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Download Model
 * =============
 */

var Download = new keystone.List('Download', {
	map: { name: 'name' },
    autokey: { path: 'slug', from: 'os version', unique: true }
});

Download.add({
	name   : { type: String, required:true},
	file: { type: Types.LocalFile, default:null, dest:'public/downloads',  filename: function(item, file) {
		return item.id+"."+file.extension
	}, required: true, index:true},

	version: { type: Types.Select, numeric:true, options:'0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8' , default:'0.1', required:true, index:true},
	os:      { type: Types.Select, options:"Windows, Windows x64, Linux x64, IBM AIX, HP NonStop Guardian TNS/X, HP NonStop Guardian TNS/E, HP NonStop Guardian TNS/R, Solarix x86", default: "Windows", required:true, index:true},
	hits:    { type: Types.Number, default:0}
//	
});

Download.defaultColumns = 'name, file.originalname, version, os, hits';
Download.register();
